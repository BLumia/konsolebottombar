#include "dfmbashplugin.h"

#include <QDebug>

#include <KParts/ReadOnlyPart> // ??
#include <KPluginFactory>
#include <KPluginLoader>
#include <KService>
#include <kde_terminal_interface.h> // https://api.kde.org/frameworks/kparts/html/classTerminalInterface.html

DFMBashPlugin::DFMBashPlugin(QObject *parent) : QObject(parent)
{
}

DFMBashPlugin::~DFMBashPlugin()
{
}

// https://github.com/KDE/dolphin/blob/eab70b2ac4a8be16a05a015f973527686ed65ae7/src/panels/terminal/terminalpanel.cpp
QWidget *DFMBashPlugin::createBashWidget(QWidget *parent)
{
    Q_UNUSED(parent);
    KPluginFactory* factory = nullptr;
    KService::Ptr service = KService::serviceByDesktopName(QStringLiteral("konsolepart"));
    if (service) {
        factory = KPluginLoader(service->library()).factory();
    }
    m_konsolePart = factory ? (factory->create<KParts::ReadOnlyPart>(this)) : nullptr;
    if (m_konsolePart) {
        connect(m_konsolePart, &KParts::ReadOnlyPart::destroyed, this, &DFMBashPlugin::bashExited);
        m_terminalWidget = m_konsolePart->widget();
        m_terminal = qobject_cast<TerminalInterface*>(m_konsolePart);

        return m_terminalWidget;
    } else {
        // failed to load.
    }

    return nullptr;
}

void DFMBashPlugin::bashExited()
{
    qDebug() << "<<<<<<<<< KParts::ReadOnlyPart::destroyed";
    m_terminal = nullptr;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(DFMBashPlugin, DFMBashPlugin)
#endif // QT_VERSION < 0x050000
