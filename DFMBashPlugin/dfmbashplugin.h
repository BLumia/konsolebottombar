#ifndef DFMBASHPLUGIN_H
#define DFMBASHPLUGIN_H

#include "dfmbashinterface.h"
#include <QGenericPlugin>

class TerminalInterface;

namespace KParts {
    class ReadOnlyPart;
}

class DFMBashPlugin : public QObject, DFMBashInterface
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID DFMBashInterface_iid FILE "DFMBashPlugin.json")
#endif // QT_VERSION >= 0x050000
    Q_INTERFACES(DFMBashInterface)

public:
    DFMBashPlugin(QObject *parent = 0);
    ~DFMBashPlugin();

    QWidget *createBashWidget(QWidget *parent = nullptr);

public slots:
    void bashExited();

private:
    KParts::ReadOnlyPart* m_konsolePart;
    QWidget* m_terminalWidget = nullptr;
    TerminalInterface *m_terminal = nullptr;
};

#endif // DFMBASHPLUGIN_H
