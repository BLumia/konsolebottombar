#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dfmbashinterface.h"
#include <QMainWindow>
#include <QSplitter>

class TerminalInterface;

namespace Ui {
    class MainWindow;
}

namespace KParts {
    class ReadOnlyPart;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void tryLoadKonsolepart();
    bool loadPlugin();

    QSplitter vertSpliter;
    DFMBashInterface *bashPluginInstance = nullptr;

private:
    Ui::MainWindow *ui;
    QWidget* m_terminalWidget = nullptr;
};

#endif // MAINWINDOW_H
