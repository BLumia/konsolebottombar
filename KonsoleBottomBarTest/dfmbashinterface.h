#ifndef DFMBASHINTERFACE_H
#define DFMBASHINTERFACE_H

#include <QWidget>


class DFMBashInterface
{
public:
    virtual ~DFMBashInterface() {}
    virtual QWidget *createBashWidget(QWidget *parent = nullptr) = 0;
};

#define DFMBashInterface_iid "io.deepin.dde-file-manager.DFMBashInterface"

Q_DECLARE_INTERFACE(DFMBashInterface, DFMBashInterface_iid)

#endif // DFMBASHINTERFACE_H
