#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLabel>
#include <QDir>
#include <QPluginLoader>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QLabel * testLabel = new QLabel("test", &vertSpliter);
    testLabel->setStyleSheet("background: gray");
    vertSpliter.setOrientation(Qt::Vertical);
    vertSpliter.addWidget(testLabel);
    ui->verticalLayout->addWidget(&vertSpliter);

    tryLoadKonsolepart();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::tryLoadKonsolepart()
{
    if (loadPlugin()) {
        m_terminalWidget = bashPluginInstance->createBashWidget(this);
        if (m_terminalWidget) {
            vertSpliter.addWidget(m_terminalWidget);
        } else {
            QLabel * testLabel = new QLabel("konsolepart", &vertSpliter);
            vertSpliter.addWidget(testLabel);
        }
    } else {
        QLabel * testLabel = new QLabel("no plugin available", &vertSpliter);
        vertSpliter.addWidget(testLabel);
    }
}

bool MainWindow::loadPlugin()
{
    QDir pluginsDir(qApp->applicationDirPath());
    pluginsDir.cd("plugins");
    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader pluginLoader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = pluginLoader.instance();
        if (plugin) {
            bashPluginInstance = qobject_cast<DFMBashInterface *>(plugin);
            if (bashPluginInstance)
                return true;
        }
    }

    return false;
}
